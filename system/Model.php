<?php
class Model
{
	/**
	 * Arguments
	 */
	public $data;
	private $proxy;
	

	/**
	 * Constructeur
	 */
	function __construct()
	{
		global $config;
		$this->data = array();
		$this->proxy = $config['proxy'];
	}
	
	/**
	 * Methodes
	 */
	function file_get_contents_proxy($url){
		// Create context stream
		$context_array = array('http'=>array('proxy'=>$this->proxy,'request_fulluri'=>true));
		$context = stream_context_create($context_array);
		// Use context stream with file_get_contents
		$data = file_get_contents($url,false,$context);
		// Return data via proxy
		return $data;
	}
	
	function saveTXT($file, $data)
	{
		$ok = false;
		$str = "";
		foreach($data as $k => $v)
		{
			$str .= "{$k} : {$v}\n";
		}
		$handle = fopen($file, "w");
		$ok = fwrite($handle, $str);
		fclose($handle);
		return $ok;
	}
	
	function saveCSV($file, $data)
	{
		$ok = false;
		$f_exist = file_exists($file);
		$fp = fopen($file, 'a');
			if(!$f_exist)
			{
				fputcsv($fp, array_keys($_POST), ";", '"');
			}
			$ok = fputcsv($fp, $_POST, ";", '"');
		fclose($fp);
		return $ok;
	}
}

?>
