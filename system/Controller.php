<?php
class Controller
{
	/**
	 * Arguments
	 */
	//public $load;
	public $model;
	public $view;
	public $data;
	
	/**
	 * Constructeur
	 */
	function __construct($c)
	{
		// Initialiser les tableaux
		$this->data = array();
	}
	
	/**
	 * Methodes
	 */
	 function setModel($obj)
	 {
		 $this->model = new $obj;
	 }
	 
	 function setView($obj)
	 {
		 $this->view = new $obj;
	 }
	 
	 function controle($c)
	 {
		// Recuperer les donnees dans le modele
		$this->data = (method_exists($this->model,$c)) ? $this->model->$c($c) : array();
		// Recuperer autres datas
		$data2 = (method_exists($this,$c)) ? $this->$c($c) : array();
		// Fusionner les tableaux
		$this->data = array_merge($this->data, $data2);
		// Afficher la vue
		if(method_exists($this->view,$c))
		{
			$this->view->$c($this->data);
		}
		else{
			$this->view->error();
		}
	 }
	 
	 
    function __destruct() {
       
    }
}

?>
