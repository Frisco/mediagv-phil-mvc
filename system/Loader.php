<?php
class Loader
{
	/**
	 * Arguments
	 */
	
	
	
	/**
	 * Methodes
	 */
	static function load($file, $retour=false)
	{
		if(file_exists($file))
		{
			if($retour == true)
			{
				$r = file_get_contents($file);
				return $r;
			}
			else
			{	
				//include($file);
				ob_start();
				include($file);
				return ob_get_clean();
			}
		}
		else
		{
			// Error
		}
	}
}

?>
