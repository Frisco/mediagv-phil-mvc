<?php
include_once("system/Controller.php");

include_once("models/ModelMyMeteo.php");
include_once("views/ViewMyMeteo.php");

class CtrlMyMeteo extends Controller
{
	function __construct($c)
	{
		parent::__construct($c);
		$this->model = new ModelMyMeteo;
		$this->view = new ViewMyMeteo;
		$this->ctrlMeteo($c);
	}
	
	function ctrlMeteo($c)
	{
		$this->data = (method_exists($this->model, $c)) ? $this->model->$c($c) : array();
		$this->view->$c($this->data);
	}
}

?>