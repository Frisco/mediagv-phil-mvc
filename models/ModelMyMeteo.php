<?php

include_once "system/Model.php";

class ModelMyMeteo extends Model
{
	/**
	 * Arguments
	 */
	public $url;
	public $urlgeocode;

	
	/**
	 * Constructeur
	 */
	function __construct()
	{
		parent::__construct();
		
		$this->url = "http://www.prevision-meteo.ch/services/json/";
		$this->urlgeocode = "http://maps.googleapis.com/maps/api/geocode/json?";
	}
	
	/**
	 * Methodes
	 */

	function ajaxmeteo()
	{
		global $config;
		$mode = $_POST['mode'];
		switch (true)
		{
			case ($mode == "tableau" || $mode == "graph") :
				$ville = $_POST['ville'];
				$data = ($config['is_proxy']) 
					? $this->file_get_contents_proxy($this->url . $ville) 
					: file_get_contents($this->url . $ville);
			break;
			case ($mode == "geocode") :
				$latitude = $_POST['latitude'];
				$longitude = $_POST['longitude'];
				$latlng = "latlng=" . $latitude . "," . $longitude . "&sensor=false";
				$data = ($config['is_proxy']) 
					? $this->file_get_contents_proxy($this->urlgeocode . $latlng) 
					: file_get_contents($this->urlgeocode . $latlng);
			break;
		}
		return $data;
	}
	

}

?>
