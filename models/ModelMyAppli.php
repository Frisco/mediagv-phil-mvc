<?php
include_once "system/Model.php";

class ModelMyAppli extends Model
{
	/**
	 * Arguments
	 */
	

	
	/**
	 * Constructeur
	 */
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Methodes
	 */

	function accueil()
	{
		$this->data = array(
			'accueil' => "Mes données de l'accueil",
			'actu' => "Mes données actu",
			'content' => "accueil"
		);
		return $this->data;
	}
	
	function contact()
	{
		$this->data = array(
			'content' => "contact"
		);
		// Recuperation du dernier Enregistrement CSV
		$alldatas = array();
		if (file_exists('docs/contact.csv') && ($handle = fopen("docs/contact.csv", "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ";", '"')) !== FALSE) {
				$alldatas[] = $data;
			}
			fclose($handle);
			$last = count($alldatas) - 1;
			foreach($alldatas[0] as $k => $v)
			{
				$this->data[$v] = $alldatas[$last][$k];
			}
		}
		
		return $this->data;
	}
	
	function form_contact()
	{
		$this->data = array(
			'content' => "form_contact"
		);
		$this->data = array_merge($this->data, $_POST);

		// Stockage Persistant dans un fichier TXT
		$this->data['form_ok'] = $this->saveTXT('docs/contact.txt', $_POST);
		
		// Stockage Persistant dans un fichier CSV
		$this->data['form_ok'] = $this->saveCSV('docs/contact.csv', $_POST);

		return $this->data;
	}
	
	function meteo()
	{
		$this->data = array(
			'content' => "meteo"
		);
		return $this->data;
	}
}

?>
