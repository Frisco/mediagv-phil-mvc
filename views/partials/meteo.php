<style>

	body{
		font-size:16px;
	}
	table#prev_jours{
		width:568px;
		font-size:1em;
		border:0px;
	}
	table#prev_jours tr:nth-child(1)
	{
		font-size:1.5em;
		text-align:center;
	}
	table#prev_jours tr:nth-child(2n)
	{
		background-color:#99FFFF;
	}
	table#prev_jours tr:nth-child(2n+1)
	{
		background-color:#99FF99;
	}
	table#prev_jours tr td:nth-child(2),
	table#prev_jours tr td:nth-child(4),
	table#prev_jours tr td:nth-child(5)
	{
		text-align:center;
	}
</style>

<script src="http://code.highcharts.com/highcharts.js"></script>

<script>
$(document).ready(function() {

	var ville = "toulouse"; // initialise à toulouse...
	var series = [];
	var categories = [];

	$("button").click(function(){
	
		var monbouton = $(this).attr('id');
	
		switch(monbouton){
			case "bouton":
				ville = $("#ville option:selected").val();
				myAjax({"ville" : ville});
			break;
			case "bouton2":
				ville = $("#cherche").val();
				myAjax(
					{
						"ville" : ville,
						"mode" : "tableau",
						"url"	: "ajaxmeteo"
					}
				);
			break;
			case "bouton3":
				ville = $("#cherche").val();
				myAjax(
					{	
						"ville" : ville,
						"mode"	: "graph",
						"url"	: "ajaxmeteo"
					}
				);
			break;
			default:
			break;
		}

	});

function myAjax(obj){
	$.ajax({
		url : obj.url,
		type : 'POST',
		dataType : 'json',
		async	:	true,
		data: 
			{
				'ville'	: obj.ville,
				'mode' 	: (obj.mode) ? obj.mode : "",
				'latitude'	: (obj.latitude) ? obj.latitude : "",
				'longitude'	: (obj.longitude) ? obj.longitude : ""
			},
		success : function(result){
			ajaxSuccess(result);			
		},
		error : function(result){
			ajaxError(result);
		},
		complete : function(result){
			ajaxComplet(result);
		}
	});
}

function ajaxSuccess(o){
	if (o.type!=undefined && o.type=='graph')
	{
		graphTemperature(o.json);
	}
	else if (o.type!=undefined && o.type=='geocode')
	{
		geocodeTableauPrev(o.json);
	}
	else  if (o.type!=undefined && o.type=='tableau'){
		tableauPrev(o.json);
	}
	else{}
}	
	
function ajaxError(o){
	console.log(o);
	alert('pb de connexion');
}	

function ajaxComplet(o){
	console.log('fini');
}

function geocodeTableauPrev(o){
	var ville = o.results[2].address_components[1].long_name;
	$("div#infoposition").html(ville);
	$("input#cherche").val(ville);
	myAjax(
		{
			"ville" : ville,
			"mode" : "tableau",
			"url"	: "ajaxmeteo"
		}
	);
}

function tableauPrev(o){
	var contents = "";
	contents+="<h1>"+o.city_info.name+"</h1>";
	contents+="<table id='prev_jours'><tr><td>Jour</td><td>Icône</td><td>Cond.</td><td>Tmin</td><td>Tmax</td></tr>";
	for(i=0;i<5;i++)
	{
		contents+=
			'<tr>'+
			'<td>'+o['fcst_day_'+i]['day_long']+'</td>'+
			'<td><img src="'+o['fcst_day_'+i]['icon']+'"/></td>'+
			'<td>'+o['fcst_day_'+i]['condition']+'</td>'+
			'<td>'+o['fcst_day_'+i]['tmin']+'°C</td>'+
			'<td>'+o['fcst_day_'+i]['tmax']+'°C</td>'+
			'</tr>';
	}
	contents+="</table>";
	
	$("div#madiv").html(contents);
	return;
}

function graphTemperature(o){
	var addCat = (categories.length > 0) ? false : true;
	var dataMin = [];
	var dataMax = [];
	var ville = o.city_info.name;
	if(addCat)
	{
		for(i=0;i<5;i++) 
		{
		 categories.push(o['fcst_day_'+i]['day_short']);
		}
	}
	
	for(i=0;i<5;i++) 
	{
		dataMin.push(o['fcst_day_'+i]['tmin']);
		dataMax.push(o['fcst_day_'+i]['tmax'])
	};
		
	
	series=[
	{
		name: ville + " (Min)",
		data: dataMin
	},
	{
		name: ville + " (Max)",
		data: dataMax
	}];


		$('#container').highcharts({
			chart: {
				type: 'line'
				//type: 'column'
			},
			title: {
				text: 'Temperature'
			},
			subtitle: {
				text: 'Source: Meteo'
			},
			xAxis: {
				categories: categories
			},
			yAxis: {
				title: {
					text: 'Temperature (°C)'
				}
			},
			plotOptions: {
				line: {
					dataLabels: {
						enabled: true
					},
					enableMouseTracking: false
				}
			},
			series: series
		}); 
	};

	function maPosition(position) {
	  var infopos = "";
	//  infopos += "Position d&#233;termin&#233;e :<br/>\n";
	  infopos += "Latitude : "+position.coords.latitude +"<br/>\n";
	  infopos += "Longitude: "+position.coords.longitude+"<br/>\n";
	//  infopos += "Altitude : "+position.coords.altitude +"<br/>\n";

	myAjax(
		{	
			"mode"	: "geocode",
			"url"	: "ajaxmeteo",
			"latitude" : position.coords.latitude,
			"longitude" : position.coords.longitude
		});
	  
	}

	if(navigator.geolocation)
	{
	  navigator.geolocation.getCurrentPosition(maPosition);
	}	
});


</script>

<h1>Météo</h1>
<input id="cherche" value=""/>
<button id="bouton2">Rechercher</button>
<button id="bouton3">Graph</button>
<br/>
<hr/>
<div id="madiv" style="max-width: 600px;min-height:100px;">

</div>

<div id="container" style="">

</div>