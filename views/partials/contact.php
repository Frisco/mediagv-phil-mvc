<script>
var Personne = {
	/**
	 * Arguments
	 */
	civilite	: "",
	nom			: "",
	prenom		: "",
	adresse1	: "",
	adresse2	: "",
	cp			: "",
	ville		: "",
	
	email		: "",
	tel			: "",
	
	message		: "",

	erreur		:false,

	// Accesseurs
	/**
	 * Setter
	 */
	setCivilite: function(val){
		var ok = false;
		if (typeof val == "string" && (val.toLowerCase() == "m" || val.toLowerCase() == "mme")) {
			this.civilite = val; ok = true;
		}
		return ok;
	},	
	setNom: function(val){
		var ok = false;
		if(this.isAlpha(val) && val != ""){
			this.nom = val; ok = true;
		}
		return ok;
	},
	setPrenom: function(val){
		var ok = false;
		if(this.isAlpha(val) && val != ""){
			this.prenom = val; ok = true;
		}
		return ok
	},
	setAdresse1: function(val){
		var ok = false;
		if(this.isAlphaNumerique(val) && val != ""){
			this.adresse1 = val; ok = true;
		}
		return ok
	},
	setAdresse2: function(val){
		var ok = false;
		if(this.isAlphaNumerique(val) || val == ""){
			this.adresse2 = val; ok = true;
		}
		return ok
	},
	setCP: function(val){
		var ok = false;
		if(this.isNumerique(val) && val != ""){
			this.cp = val; ok = true;
		}
		return ok
	},
	setVille: function(val){
		var ok = false;
		if(this.isAlpha(val) && val != ""){
			this.ville = val; ok = true;
		}
		return ok
	},

	setEmail: function(val){
		var ok = false;
		if(this.isEmail(val) && val != ""){
			this.email = val; ok = true;
		}
		return ok
	},
	setTel: function(val){
		//var ok = false;
		//if(this.isTel(val) && val != ""){
			this.tel = val; ok = true;
		//}
		return ok
	},
	setMessage: function(val){
		//var ok = false;
		//if(this.isAlpha(val) && val != ""){
			this.message = val; ok = true;
		//}
		return ok
	},	
	
	setErreur: function(bool){
		this.erreur = bool;
	},
	getErreur: function(){
		return this.erreur;
	},
	/**
	 * Getter
	 */
	getCivilite: function(){
		return this.civilite;
	},	
	getNom: function(){
		return this.nom;
	},
	getPrenom: function(){
		return this.prenom;
	},
	getAdresse1: function(){
		return this.adresse1;
	},
	getAdresse2: function(){
		return this.adresse2;
	},
	getCP: function(){
		return this.cp;
	},
	getVille: function(){
		return this.ville;
	},
	getEmail: function(){
		return this.email;
	},
	getTel: function(){
		return this.tel;
	},
	getMessage: function(){
		return this.message;
	},
	
	/**
	 * Methodes
	 */
	 setAllForm: function(params){
		this.setErreur(false);
		this.setCivilite(params.civilite) ? this.notError('civilite') : this.error('civilite');
		this.setNom(params.nom) ? this.notError('nom') : this.error('nom');
		this.setPrenom(params.prenom) ? this.notError('prenom') : this.error('prenom');
		this.setAdresse1(params.adresse1) ? this.notError('adresse1') : this.error('adresse1');
		this.setAdresse2(params.adresse2) ? this.notError('adresse2') : this.error('adresse2');
		this.setCP(params.cp) ? this.notError('cp') : this.error('cp');
		this.setVille(params.ville) ? this.notError('ville') : this.error('ville');
		this.setEmail(params.email) ? this.notError('email') : this.error('email');
		this.setTel(params.tel) ? this.notError('tel') : this.error('tel');
		this.setMessage(params.message) ? this.notError('message') : this.error('message');
	},
	afficherHTML : function(obj){  
		var identite = this.civilite + " ";
		identite+= this.nom + " ";
		identite+= this.prenom + " ";
		identite+= "<br/>";
		identite+= this.adresse1 + " ";
		identite+= "<br/>";
		identite+= this.adresse2 + " ";
		identite+= (this.adresse2 != "") ? "<br/>" : "";
		identite+= this.cp + " ";
		identite+= this.ville;
		obj.innerHTML = identite;
	},
	error : function(o){
		obj = document.getElementById(o).parentNode.parentNode;
		obj.style.backgroundColor = "#FF7373";
		obj.style.border = "1px solid #FF2626";
		this.setErreur(true);
		
	},
	notError : function(o){
		obj = document.getElementById(o).parentNode.parentNode;
		obj.style.backgroundColor = "transparent";
		obj.style.border = "0px";
	},
	isAlpha : function(str){
		var regex = /^[a-zA-Z\- ]*$/;
        return regex.test(str);
	},
	isAlphaNumerique : function(str){
		var regex = /^[a-zA-Z0-9_\-\. ]*$/;
        return regex.test(str);
	},
	isNumerique : function(str){
		var regex = /^[0-9\.]*$/;
        return regex.test(str);
	},
	isTel : function(str){
		var regex = /^0[67]([-\/. ]?[0-9]{2}){4}$/;
        return regex.test(str);
	},
	isEmail : function(str){
		var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
        return regex.test(str);
	}
}

function valider(){
	// Block destination du résultat
	var resultat = document.getElementById("resultat");
	// On crée un nouvel objet 
	var person = Object.create(Personne);
	person.setAllForm({
		civilite 	: $("form > div select[name='civilite']").val(),
		nom 		: $("form > div input[name='nom']").val(),
		prenom		: $("form > div input[name='prenom']").val(),
		adresse1	: $("form > div input[name='adresse1']").val(),
		adresse2	: $("form > div input[name='adresse2']").val(),
		cp			: $("form > div input[name='cp']").val(),
		ville		: $("form > div input[name='ville']").val(),
		email		: $("form > div input[name='email']").val(),
		tel			: $("form > div input[name='tel']").val(),
		message		: $("form > div textarea[name='message']").val(),
	});
	// On affiche le resultat s'il n'y a pas d'erreur
//	(!person.getErreur()) ? person.afficherHTML(resultat) : null;
	if (!person.getErreur()) $("form").submit();
}			
</script>
<style>
	form > div
	{
		border: 0px solid red;
		float: left;
		width:100%;
		display:inline-block;
		margin-bottom: 5px;
		padding:2px;
	}
	form > div label
	{
		width:100px;float:left;
	}
	form > div sup
	{
		color : #FF4C4C;
	}
	form input[name="nom"],
	form input[name="prenom"],
	form input[name="adresse1"],
	form input[name="adresse2"],
	form input[name="ville"],
	form input[name="email"],
	form input[name="tel"]
	{
		width:250px;
	}
		form textarea[name="message"]
	{
		width:250px;
		height : 75px;
	}
	hr{
		border:0px;
		border-top : 1px solid #EFFFBF;
		float:left;
		width:100%;
	}
</style>
<script>
$(document).ready(function() {
	$("button").click(function()
	{
		switch($(this).attr('id'))
		{
			case "valider":
				valider();
				
			break;
			case "reset":
			
			break;
			default:
			
			break;
		}
	});
});
</script>
<h1>Contact</h1>

<form action="form_contact" method="post">
	<br/>
	<div>
		<label>Civilité<sup>*</sup> : </label>
		<div>
			<select name="civilite" id="civilite">
				<option value=""></option>
				<option value="Mme" <?= (isset($civilite) && $civilite=="Mme")?"selected = 'selected'":"" ?>>Mme</option>
				<option value="M" <?= (isset($civilite) && $civilite=="M")?"selected = 'selected'":"" ?>>M</option>
			</select>
		</div>
	</div>	
	<div>
		<label>Nom<sup>*</sup> : </label>
		<div><input name="nom" value="<?= (isset($nom))?$nom:"" ?>" id="nom" placeholder="Votre nom" /></div>
	</div>
	<div>
		<label>Prénom<sup>*</sup> : </label>
		<div><input name="prenom" value="<?= (isset($prenom))?$prenom:"" ?>" id="prenom" placeholder="Votre prénom" /></div>
	</div>
	<hr/>
	<div>
		<label>Adresse (1)<sup>*</sup> : </label>
		<div><input name="adresse1" id="adresse1" value="<?= (isset($adresse1))?$adresse1:"" ?>" placeholder="Votre adresse" /></div>
	</div>
	<div>
		<label>Adresse (2) : </label>
		<div><input name="adresse2" id="adresse2" value="<?= (isset($adresse2))?$adresse2:"" ?>" placeholder="Votre adresse suite" /></div>
	</div>
	<div>
		<label>Code Postal<sup>*</sup> : </label>
		<div><input name="cp" id="cp" value="<?= (isset($cp))?$cp:"" ?>" placeholder="Code postal" /></div>
	</div>
	<div>
		<label>Ville<sup>*</sup> : </label>
		<div><input name="ville" id="ville" value="<?= (isset($ville))?$ville:"" ?>" placeholder="Ville" /></div>
	</div>	
	<hr/>
	<div>
		<label>Email<sup>*</sup> : </label>
		<div><input name="email" id="email" value="<?= (isset($email))?$email:"" ?>" placeholder="Email" /></div>
	</div>	
	<div>
		<label>Tel : </label>
		<div><input name="tel" id="tel" value="<?= (isset($tel))?$tel:"" ?>" placeholder="Tel" /></div>
	</div>	
	<hr/>
	<div>
		<label>Message : </label>
		<div><textarea name="message" id="message" placeholder="Message"><?= (isset($message))?$message:"" ?></textarea></div>
	</div>	
	<hr/>
	<div>
		<sup>*</sup> Champs obligatoires
	</div>
	<hr/>
	<div>
		<button type="button" id="valider">Valider</button>
	</div>
</form>
<br/>
