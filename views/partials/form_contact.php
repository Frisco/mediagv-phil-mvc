<h1>Contact </h1>
<br/>
<?= $civilite ?> <?= $nom ?> <?= $prenom ?>,
<br/><br/>
<?php if($form_ok) { ?>
	Votre demande a bien été enregistrée !
<?php } else { ?>
	Suite à un problème serveur,  votre demande n'a pas été enregistrée !
	<br/>
	Veuillez essayer ultérieurement.
	<br/>
	Merci pour votre compréhension.
<?php } ?>

<br/>

