<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<style>
body{
	font-size:16px;
	font-family: 'Roboto Condensed', sans-serif;
}
div{
	margin:auto;
}
header{
	background-color:#FFB973;
	width:96%;
	height:55px;
	padding:2%;
	
}

section{
	width:71%;
	background-color:#FFCC99;
	min-height:500px;
	height:auto;
	float:left;
	padding:2%;
	
}
aside{
	width:21%;
	background-color:#FFE599;
	min-height:500px;
	float:right;
	padding:2%;
}
footer{
	width:96%;
	background-color:#CCFF99;
	height:100px;
	float:left;
	padding:2%;
}
main{
	display: flex;
	flex-wrap: wrap;
}
</style>
</head>
<body>
<div style="width:800px;">

<header> 
<?php include "views/partials/header.php"; ?>
</header>
<main>
<section>
<?php include "views/partials/section.php"; ?>
</section>

<aside>
<?php include "views/partials/aside.php"; ?>
</aside>
</main>
<footer>
<?php include "views/partials/footer.php"; ?>
</footer>

</div>

</body>
</html>