<?php
include_once "system/View.php";

class ViewMyMeteo extends View
{
	/**
	 * Arguments
	 */

	
	/**
	 * Constructeur
	 */
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Methodes
	 */

	function ajaxmeteo($json)
	{
		$retour = "";
		if($_POST['mode'] == "graph")
			$retour = '{"type":"graph","json":'.$json.'}';
		else if($_POST['mode'] == "tableau")
			$retour = '{"type":"tableau","json":'.$json.'}';
		else if($_POST['mode'] == "geocode")
			$retour = '{"type":"geocode","json":'.$json.'}';
		else{}
		echo $retour;
	}

	function error()
	{
		$this->afficheVue(array(),"views/error");
	}
}

?>
