<?php
include_once "system/View.php";

class ViewMyAppli extends View
{
	/**
	 * Arguments
	 */

	
	/**
	 * Constructeur
	 */
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Methodes
	 */

	function accueil($data)
	{		
		$this->afficheVue($data,"views/default");
	}
	
	function contact($data)
	{		
		$this->afficheVue($data,"views/default");
	}
	
	function form_contact($data)
	{
		$this->afficheVue($data,"views/default");
	}
	
	function meteo($data)
	{
		$this->afficheVue($data,"views/default");
	}

	function error()
	{
		$this->afficheVue(array(),"views/error");
	}
}

?>
